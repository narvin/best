Best
====

Best is a library of functions to aid in unit-testing Bash scripts. You can
use it to assert that actual values are equal to an expected ones, and to
print sections with nicely formatted headers, results, and summaries.

![Example 1](image/example-1.png)

Installation
============

Source `best` in your unit test files.

```Shell
# Inside of a unit test file
source /path/to/best
```

Usage
=====

To create the output pictured above, first we create a section of tests
where we manually compute (not shown) each outcome, and display the results
and tallies.

```Shell
best_summary_heading 'foo tests'
best_result OK 'It does something simple.'
best_result FAIL 'It does something hard.'
best_result WARN 'It does something weird.'
best_result TODO "It doesn't do this yet."
best_result UNK 'WTF!'
best_summary
```

Next, we create a section of tests where we use best functions to compare
expressions and values, and display the results and tallies.

```Shell
best_summary_heading 'bar tests'
best_strings_eq_result "$(echo ~)" "/home/$(whoami)" 'Standard home directory'
best_numbers_eq_result "$(id | cut -c 5- | cut -d '(' -f 1)" 1000 'Main user'
declare -a a=(a b c 1 2 3 foo bar) b=(a b c 1 2 3 foo bar)
best_arrays_eq_result a b 'Arrays match'
best_summary
```

Finally, we show the grand totals for the two previous sections.

```Shell
best_total_summary
```

best\_result
------------

```Shell
best_result <result> <message>
```

`best_result` prints the `result` in brackets, formatted, and color-coded,
followed by the `message`. Results are tallied in green, red, yellow, and
green categories. `OK` and `PASS` are green. `FAIL` is red. `WARN` and `TODO`
are yellow. All other results are grey.

This function is useful if you've already computed the outcome of a test
and now want print the result, and add it to the appropriate tally.

best\_heading
-------------

```Shell
best_heading <heading> [<prefix>='*** '] [<suffix>=' ***']
```

`best_heading` prints the `heading` surrounded by the `prefix` and
`suffix`. You would typically use this function before a group of related
tests.

best\_summary\_group
--------------------

```Shell
best_summary_group
```

`best_summary_group` resets the group tally, but not the total tally. You
would use this function at the beginning of a group of tests before printing
any results.

best\_summary\_heading
----------------------

```Shell
best_summary_heading <heading> [<prefix>='*** '] [<suffix>=' ***']
```

`best_summary_heading` combines `best_heading` and `best_summary_group`.

best\_summary
-------------

```Shell
best_summary
```

`best_summary` prints the group tally. You would typically use this function
after printing the results for a group of tests.

best\_total\_summary
--------------------

```Shell
best_total_summary [<heading>=TOTAL] [<prefix>='*** '] [<suffix>=' ***']
```

`best_total_summary` prints a `heading` surrounded by a `prefix` and `suffix`,
followed by the total tally for all groups of tests. You would typically
use this function after printing the results for all groups of tests.

best\_assert\_strings\_eq
-------------------------

```Shell
best_assert_strings_eq <actual> <expected>
```

`best_assert_strings_eq` does a case-sensitive comparison of the strings
`actual` and `expected`. It returns an exit code of 0 if the strings are
equal. It prints the mismatched strings and returns an exit code of 64 if
the strings are not equal.

best\_strings\_eq\_result
-------------------------

```Shell
best_string_eq_result <actual> <expected> <message> [<exp_res>=] [<exp_code>=0]
```

`best_strings_eq_result` compares two strings using
`best_assert_strings_eq`. If the output and return code from
`best_assert_strings_eq` match `exp_res` and `exp_code`, then the function uses
`best_result` to print an OK `message`, otherwise it prints a FAIL `message`.

best\_assert\_numbers\_eq
-------------------------

```Shell
best_numbers_eq <actual> <expected>
```

`best_assert_numbers_eq` does a floating-point comparison of the numbers
`actual` and `expected`, up to the precision of `expected`. It returns an
exit code of 0 if the numbers are equal. It prints the unequal numbers and
returns an exit code of 64 if the strings are not equal.

best\_numbers\_eq\_result
-------------------------

```Shell
best_numbers_eq_result <actual> <expected> <message> [<exp_res>=] [<exp_code>=0]
```

`best_numbers_eq_result` compares two numbers using
`best_assert_numbers_eq`. If the output and return code from
`best_assert_numbers_eq` match `exp_res` and `exp_code`, then the function uses
`best_result` to print an OK `message`, otherwise it prints a FAIL `message`.

best\_assert\_arrays\_eq
-------------------------

```Shell
best_assert_arrays_eq <actual> <expected>
```

`best_assert_arrays_eq` compares the two arrays *named by* `actual` and
`expected` by reference. It returns an exit code of 0 if the strings are
equal. It prints the mismatched strings and returns an exit code of 64 if
the strings are not equal.

**Note:** This function uses name references, so if you use it, your
code can't contain variables named `_best_assert_arrays_eq_act` or
`_best_assert_arrays_eq_exp`.

best\_arrays\_eq\_result
-------------------------

```Shell
best_arrays_eq_result <actual> <expected> <message> [<exp_res>=] [<exp_code>=0]
```

`best_arrays_eq_result` compares the two arrays *named by* `actual`
and `expected` by reference. If the output and return code from
`best_assert_arrays_eq` match `exp_res` and `exp_code`, then the function uses
`best_result` to print an OK `message`, otherwise it prints a FAIL `message`.

**Note:** This function uses `best_assert_arrays_eq`, which uses name
references, so if you use it, your code can't contain variables named
`_best_assert_arrays_eq_act` or `_best_assert_arrays_eq_exp`.

best\_unset\_e and best\_restore\_e
-----------------------------------

```Shell
best_unset_e

# Some code that may produce an error where you want to capture the exit code
...

best_restore_e
```

`best_unset_e` will turn off the `e` option and remember if it had been
turned on. `best_restore_e` will turn the `e` option back on if it had been
turned on before `best_unset_e` turned it off. These functions are used by
some result functions so they can capture non-zero exit codes in the event
that the `e` option is set.

